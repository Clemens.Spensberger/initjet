!-------------------------------------------------------------
! Module for calculating moisture related things
!   
! For more info see the README file and/or documentation.
!--------------------------------------------------------------


module moist

USE const 
USE io
USE init
implicit none

CONTAINS 

!------------------------------------------------------------------------------
subroutine get_rh(rh)
   !create relative humidity profile
   real(kind=8),intent(inout)   :: rh(ny,nz)
   real(kind=8)   :: rh_prof(nz)

   call get_rh_vert(rh_prof)

    !same rh-profile everywhere
    do j=1,ny
       rh(j,:)=rh_prof
    enddo

end subroutine get_rh
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
subroutine get_rh_vert(rh_prof)
   !create relative humidity profile
   real(kind=8),intent(out)   :: rh_prof(nz)
   real(kind=8)            :: dRHdz

     if(rh_type==1)then  !create a linear profile
      dRHdz=(RH_max-RH_min)/RH_hgt
      do k=1,nz
         z=(k-1)*dz
            if(z.lt.RH_hgt)then
               rh_prof(k)=RH_max-dRHdz*z
            else
               rh_prof(k)=RH_min
            endif
         enddo
    endif

   if(rh_type==2)then !create log rh profile
        do k=1,nz
            z=(k-1)*dz
            rh_prof(k)=RH_max*exp(-0.5*(z/RH_hgt)**RH_shape)
        enddo
    endif
end subroutine get_rh_vert
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
! Update all fields that need to be updated after the addition/adaption of relative humidity
! In this process, pre-existing temperatures are reinterpreted as virtual temperatures to not
! affect density (which would require a hydrostatic/geostrophic rebalancing)
!
! (used be to be called "calc_all_3D" in func.f90)
subroutine update_fields_to_rh(tt,ttv,th,thv,qq,rh,pp)
   real(kind=8), intent(in), dimension(ny,nz) :: rh, pp
   real(kind=8), intent(inout), dimension(ny,nz) :: tt,ttv,th,thv,qq

   ! Re-interpret temperatures as virtual temperatures to maintain balances
   ttv = tt
   thv = th

   ! Recalculate tt and qq based on ttv, rh, pp
   do k=1,nz
      do j=1,ny
         call calc_tt_qq(ttv(j,k),rh(j,k),pp(j,k), tt(j,k),qq(j,k))
      enddo
   enddo
   
   ! Update potential temperature by new tt
   th = tt * (p00/pp)**(Rd/cp)
end subroutine
!------------------------------------------------------------------------------


!-------------------------------------------------------
! Calc. saturation vapor pressure given temperature
! Hai: changed to Buck's equation
function calc_es(tt) result (es)
   real(kind=8), intent(in) :: tt
   real(kind=8) :: es, Tc

   Tc = tt - 273.15
   es = 611.21*exp((18.678 - Tc/234.5)*(Tc/(257.14+Tc))) 
end function
!-------------------------------------------------------


!-------------------------------------------------------
! Calc. relative humidity based on temperature, specific humidity and pressure
! (based on function by Hai)
function calc_rh(tt,qq,pp) result (rh)
   real(kind=8) , intent(in) :: tt,qq,pp
   real(kind=8)  :: rh, es, qqs

   rh = 0.0
   es = calc_es(tt)
   qqs = (Rd/Rv)*(es/pp)
   rh = qq/qqs
end function
!-------------------------------------------------------


!------------------------------------------------------- 
! Calc. specific humidity from temperature, relative humidity and pressure 
! (based on function by Hai)
function calc_qq(tt,rh,pp) result (qq)
   real(kind=8) , intent(in) :: tt,rh,pp
   real(kind=8) :: qq
   real(kind=8)  :: es, qqs

   es = calc_es(tt)
   qqs = (Rd/Rv)*(es/pp)
   qq = rh*qqs
end function
!-------------------------------------------------------


!-------------------------------------------------------
! Hai: Use bisection method to get temperature and specific humidity from tt and rh
subroutine calc_tt_qq(ttv,rh,pp, tt,qq)
   real(kind=8), intent(in) :: ttv, rh, pp
   real(kind=8), intent(out) :: tt, qq
   real(kind=8), parameter :: eps=1e-5  ! threhold for error, 1e-5 --> error of 0.00001 degree
   real(kind=8) :: delta, tt_old
   integer :: i

   tt = ttv ! First guess
   delta = 999.9 
   i=1
   do while(delta .gt. eps)
      qq = calc_qq(tt,rh,pp)
      tt_old = tt  
      tt = ttv*(Rd/Rv)*(1+qq)/(qq+(Rd/Rv))
      delta = abs(tt-tt_old)
   end do
end subroutine
!-------------------------------------------------------



end module moist
