!-------------------------------------------------------------
! Main program for generating initial conditions 
!   for the idealized baroclinic wave test-case in WRFV3.4
!
! For more info see the README file and/or documentation.
! 
!   Annick Terpstra, 2013
!--------------------------------------------------------------

program main

USE const
USE io
USE init 
USE bc
USE jet
USE moist
USE pert
USE func

implicit none

!fields for initial conditions
real(kind=8), allocatable, dimension(:,:,:) :: tt,ttv,rho,pp,uu,vv,rh,qq,th,thv,zz
real(kind=8), allocatable, dimension(:,:) :: sst, sst2, seaice, seaice2, tsk
real(kind=8), allocatable, dimension(:) :: ff, ee

!fields for calculations of jet
real(kind=8), allocatable, dimension(:,:)   :: coeff,uu_jet,pp_jet,th_jet,rho_jet
real(kind=8), allocatable, dimension(:)     :: soll, ipiv
real(kind=8), allocatable, dimension(:)     :: p_vert,th_vert,rho_vert
integer :: info


!-------------------------------------------------------------------
call get_nml()      !read the namelist
call domainspec()   !calculate some domain specifics
call consistcheck()   !check initialization
!--------------------------------------------------------------------


if(.not.bouss)then
   !*************************
   !**** FULL TW-balance ****
   !*************************
   if(debug) print*,"method: full TW-balance"
   !allocate space for calculations
   allocate(uu_jet(ny,nz),pp_jet(ny,nz),rho_jet(ny,nz))
   allocate(ff(ny),ee(ny))
   allocate(soll(ny*nz),coeff(ny*nz,ny*nz))
   allocate(p_vert(nz),rho_vert(nz))   !vertical pressure profile
   allocate(ipiv(ny*nz))   !extra array for pivoting...
   allocate(pp(nx,ny,nz),uu(nx,ny,nz),rho(nx,ny,nz),tt(nx,ny,nz),th(nx,ny,nz))
   
   
   !-------------------------------------------------------------------
   call get_coriolis(ff,ee)   ! Set coriolis parameter
   call get_jet(uu_jet)    ! Obtain windfield
   call get_bc_vert(p_vert,rho_vert)   !get the vertical boundary condition
   call fill_matrix(coeff,soll,uu_jet,p_vert,ff)   !fill the matrix
   call dgesv(ny*nz, 1, coeff, ny*nz, ipiv, soll, ny*nz, info)   !solve for pressure (LAPACK)
   !----------------------------------------------------------------------
   
   !***PRESSURE***
   !retrieve pressure
   do k=1,nz
      do j=1,ny
         pp_jet(j,k)=soll((k-1)*ny+j)
      enddo
   enddo
   
   if(debug_profiles) call print_profile_m(pp_jet,uu_jet,dz)
   
   !clean up 
   deallocate(soll,coeff,ipiv,p_vert)
   
   !fill the 3D-fields for pp and uu
   pp(1,:,:)=pp_jet(:,:)
   uu(1,:,:)=uu_jet(:,:)
   deallocate(pp_jet,uu_jet)
   
   !***DENSITY***
   ! calculate density using hydrostatic equation
   do k=2,nz-1
      do j=1,ny
         rho(1,j,k)=(pp(1,j,k+1)-pp(1,j,k-1))/(-2*dz*g)
      enddo
   enddo
   
   !density at top and bottom
   !linear extrapolation, assumption: rho_surface=rho(j,2)+ 2*(rho(j,1.5)-rho(j,2))
   do j=1,ny 
         !bottom
         rho(1,j,1)=(3D0*pp(1,j,1)-4D0*pp(1,j,2)+pp(1,j,3))/(2D0*dz*g)
      !top
      rho(1,j,nz)=(pp(1,j,nz-1)-pp(1,j,nz))/(dz*g)
      rho(1,j,nz)=rho(1,j,nz-1) + 2D0*(rho(1,j,nz)-rho(1,j,nz-1))   
   enddo
   
   
   !***TEMPERATURE***
   !calculate temperature and potential temperature
   tt(1,:,:)=pp(1,:,:)/(Rd*rho(1,:,:))
   th(1,:,:)=tt(1,:,:)*(p00/pp(1,:,:))**(Rd/cp)


else   !create initial conditions using Boussinesq-approximation
   if(debug) print*,"method: boussinesq"
   !***************************
   !**** BOUSSINESQ-APPROX ****
   !***************************
   
   !There could be/was some Boussinesq-way of creating intial conditions here.
   !Too big of a mess. If needed please refrain to older versions, and implement clean version.

   print *,'ERROR: The Boussinesq option is not yet implemented again.'
   stop 5
   
endif   !endif from choices: full TW-balance / Boussinesq-approx



!**************************************************************
!(1) Add moisture, if so requested
!    If moisture is added, all temperatures are reinterpreted as virtual temperatures
if ( debug ) print *, 'Diagnostics step 1: Add moisture'
allocate(rh(nx,ny,nz),thv(nx,ny,nz),ttv(nx,ny,nz),qq(nx,ny,nz))
if(add_moist)then
   call get_RH(rh(1,:,:))
   call update_fields_to_rh(tt(1,:,:),ttv(1,:,:),th(1,:,:),thv(1,:,:),qq(1,:,:), rh(1,:,:),pp(1,:,:))
else
   ! Set dry values
   rh(1,:,:) = 0.0
   qq(1,:,:) = 0.0
   thv(1,:,:) = th(1,:,:)
   ttv(1,:,:) = tt(1,:,:)
endif
if ( debug ) print *, 'Diagnostics step 1 complete'

!**************************************************************
!(2) Meridional winds
if ( debug ) print *, 'Diagnostics step 2: Meridional wind'
allocate(vv(nx,ny,nz)) 
vv(1,:,:)=0.0 !set vv to 0 initially
if ( debug ) print *, 'Diagnostics step 2 complete'

!**************************************************************
!(3) Geopotential height
if ( debug ) print *, 'Diagnostics step 3: Geopotential'
allocate(zz(nx,ny,nz))
do k=1,nz
   zz(1,:,k) = (k-1)*dz
enddo
if ( debug ) print *, 'Diagnostics step 3 complete'

!-------------------------------------------------------------------
if(debug_profiles) call print_sounding(uu(1,:,:),pp(1,:,:),rho(1,:,:),tt(1,:,:),th(1,:,:),rh(1,:,:),dz)

!**************************************************************
!(4) **** Convert to 3D fields
if ( debug ) print *, 'Diagnostics step 4: Make 3D'
call make_3D(uu)
call make_3D(vv)
call make_3D(zz)
call make_3D(pp)
call make_3D(rho)
call make_3d(tt)
call make_3d(ttv)
call make_3d(th)
call make_3d(thv)
call make_3d(rh)
call make_3d(qq)
if ( debug ) print *, 'Diagnostics step 4 complete'


!**************************************************************
!(5) **** SST and sea ice
if ( debug ) print *, 'Diagnostics step 5: SSTs and SICs'

! CSP: directly prepare SST and SEAICE on the mass grids to avoid interpolation
!TODO: Check ramifications for the SST/SIC fields used in ideal.exe
allocate(sst(mnx,mny), sst2(mnx,mny), seaice(mnx,mny), seaice2(mnx,mny), tsk(mnx,mny))

! Option 0: No SSTs set
if ( sfc_type.eq.0 ) then
   sst(:,:) = 0.0
   seaice(:,:) = 0.0

! Option 1: Constant offset between SST and lowest-level temperatures, defined by sst_diff
else if ( sfc_type.eq.1 ) then
   call get_sfc_by_offset(sst,seaice, tt(:,:,1))

! Option 2: Read SSTs and SICs from external high-resolution input
else if ( sfc_type.eq.2 ) then
   call  get_sfc_external(sst,seaice)

! Unknown option
else
   print *, "Warning: Unkown sfc_type: ", sfc_type
   print *, " -> all surface fields set to zero."

   sst(:,:) = 0.0
   seaice(:,:) = 0.0
endif

! Derive later SSTs and SICs from initial ones
if ( sfc_type.gt.0 .and. sfc_adjust ) then
   call get_sfc2_from_sfc(sst, sst2, sst_diff2)
   call get_sfc2_from_sfc(seaice, seaice2, 0.0_8)

! Otherwise just keep constant in time
else
   sst2(:,:) = sst(:,:)
   seaice2(:,:) = seaice(:,:)
end if

! Set initial skin temperatures: th0 over 100% sea ice, SST over open water
tsk(:,:) = (1.0-seaice(:,:)) * sst(:,:) + seaice(:,:) * th0 

if ( debug_profiles ) call print_corr_and_sst(sst,ff)
if ( debug ) print *, 'Diagnostics step 5 complete'

!**************************************************************
!(6) Add perturbation in either the pressure or temperature field if so configured
if ( debug ) print *, 'Diagnostics step 6: Add perturbation if so requested'
call get_pert(tt,th,uu,vv,rho,pp,ff)
if ( debug ) print *, 'Diagnostics step 6 complete'

!**************************************************************
! Generate output files, depending on what was requested

!Note: rh is replaced with qv now
!TODO: Add sea ice to this option
if(wrf_file) call write_wrf(uu,vv,th,rho,qq,ff,pp,sst,sst2)

!TODO: Add sst2,seaice2 to this option
if(wrf_real_file) call write_wrf_real(uu,vv,tt,rh,zz,pp,ff,ee,sst,seaice,tsk)


if(nc_file) call write_nc(uu(P_nx,:,:),pp(P_nx,:,:),rho(P_nx,:,:),tt(P_nx,:,:),rh(P_nx,:,:), &
                &         th(P_nx,:,:),thv(P_nx,:,:),ttv(P_nx,:,:),qq(P_nx,:,:),zz(P_nx,:,:),& 
                &         ff,sst,sst2,seaice,seaice2,tsk)

if(debug_profiles) call print_sounding2(uu(P_nx,:,:),pp(P_nx,:,:),rho(P_nx,:,:),tt(P_nx,:,:),&
                       &  rh(P_nx,:,:),th(P_nx,:,:),thv(P_nx,:,:),ttv(P_nx,:,:),qq(P_nx,:,:),zz(P_nx,:,:))


!-------------------------------------------------------------------
! Clean up
deallocate(uu,vv,rho,tt,ttv,th,thv,qq,rh,pp,zz,ff,ee,sst,sst2,seaice,seaice2,tsk)

if(debug) print'(/A14)',"... done ..."



end program main
